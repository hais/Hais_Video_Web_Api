package pw.hais.video.entity

import javax.persistence.*

@Entity
@Table(name = "t_user")
data class User(
        @Id @GeneratedValue(strategy = GenerationType.AUTO) var id: Long? = -1L,
        var userName: String = "",
        var passWord: String = ""
)