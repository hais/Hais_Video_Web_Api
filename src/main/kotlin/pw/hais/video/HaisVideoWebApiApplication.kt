package pw.hais.video

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class HaisVideoWebApiApplication

fun main(args: Array<String>) {
    runApplication<HaisVideoWebApiApplication>(*args)
}
