package pw.hais.video.repository

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Service
import pw.hais.video.entity.User

@Service
interface UserRepository : CrudRepository<User, Long> {

    fun findByUserName(userName: String): List<User>

    @Query("from User u where u.userName=:name")
    fun findUser(@Param("name") name: String): List<User>
}